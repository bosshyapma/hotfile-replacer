# HotFile Replacer - Edits file data to run with a program, then reverts it back.  
**HotFile Replacer** takes a command, a file, and contents to replace in that file.  
(**HFR** in this **README.md** isn't *high frame rate, a photography term*, its a short term for **(H)ot(F)ile (R)eplacer**).  

### Required applications for HFR:
  1. **cat** from [**GNU coreutils**](https://www.gnu.org/software/coreutils/)  
     *(for reading and piping the **original file**)*
  2. **sed** from [**GNU sed**](https://www.gnu.org/software/sed/)
     *(for editing the **original file** piped from **cat**)*
  3. **bash** from [**GNU bash**](https://www.gnu.org/software/bash/)
     *(for proper piping from **cat** to **sed**)*
  4. That program you will launch after *sed*ing the file (of course).

### What is the main goal of this application?  
The main goal of **HFR** is to change a file used by several apps that requires some manual edits before running.  
For example, you might want to compile your C++ program with different variable names without editing the whole source code.  
You can use this program to run *GCC* with *edited-file* without losing original file.  
See **How HFR makes this happen?** part just below to see **how HFR makes this happens**.

### How HFR makes this happen?
**HFR** follows these steps. **HFR**,  
  1. Copies original file to a temporary location.  
  2. Calls [**GNU sed **](https://www.gnu.org/software/sed/) to edit given file's data using given  **sed** instructions  
     **(up to 10, *until I learn how to create a global variable from a function*.)**.  
  3. Runs given command and waits until program closes  
     *(**IMPORTANT**: if program detaches itself immediately and  
      it has an option to not detach itself,  
      give that option to the command so **HFR** will not revert file back immediately).*  
  4. Revert file back from copy created in **Step 1**.  
  5. Deletes file in **Step 1**.  

### License:  
**HotFile Replacer** is licensed under [**GNU General Public License v3.0 (or later)** (Click to see)](https://www.gnu.org/licenses/gpl-3.0.en.html)
