#define HFR_VERSION "alpha0.1"
#include <string> // for strings
#include <iostream> // for std::cout's
#include <sstream> // for stringstreams
#include <stdlib.h> // for system()'s
#include <fstream> // for pipes

using namespace std; // shorten std:: parts
extern char _binary_target_o_start; // compiled with stuff
extern char _binary_target_o_end; // ...end

bool verbose=false, silent=false, ins_index=0;
string launchedas, file_to_swap, command_to_run;
string instructions_1,
       instructions_2,
       instructions_3,
       instructions_4,
       instructions_5,
       instructions_6,
       instructions_7,
       instructions_8,
       instructions_9,
       instructions_10;
stringstream buffer;

int launch(){
  int result;
  result = system(buffer.str().c_str());
  if (verbose) cout << "Exit status for \"" << buffer.str() << "\" is: " << result << "." << endl;
  buffer.str(""); buffer.clear();
  return result;
}

void version(){
  cout << "HotFile Replacer version: " << HFR_VERSION << " (launched as: \"" << launchedas << "\"), licensed under GNU GPL v3 (or later)." << endl <<
  "See \"https://gitlab.com/bosshyapma/hotfile-replacer\" for source code, " << endl <<
  "\"https://www.gnu.org/licenses/gpl-3.0.en.html\" for used license (GNU GPL v3 or later)." << endl << endl <<
  "This copy of HFR (\"" << launchedas << "\") is compiled using the following information:" << endl;
  char* address_data = &_binary_target_o_start;
  while ( address_data != &_binary_target_o_end ) cout << *address_data++;
  exit(0);
}

void help(){
  cout << "HotFile Replacer Help Text" << endl <<
  "--version                         / -v        : Shows version and license." << endl <<
  "--help                            / -h        : Shows this help text." << endl <<
  "--silent                          / -S        : Enables silent mode." << endl <<
  "--verbose                         / -V        : Enables verbose mode." << endl <<
  "--file <file>                     / -f <file> : File to hot-replace." << endl <<
  "--instructions <sed instructions> / -i <sed>  : Up to 10, \"sed\" instructions for editing file." << endl <<
  "--command <command>               / -c <cmd>  : THIS MUST BE THE LAST ARGUMENT" << endl <<
  "                                                Specifies command to run after editing the file." << endl;
  exit(0);
}

void argument_check(int argc, char *argv[]){
  string current_argument;
  for (int i = 1; i < argc; ++i){
    current_argument = argv[i];
    if (current_argument == "--version" || current_argument == "-v") version();
    else if (current_argument == "--help" || current_argument == "-h") help();
    else if (current_argument == "--verbose" || current_argument == "-V") { 
      verbose = true; silent = false;
    }
    else if (current_argument == "--silent" || current_argument == "-S") {
      silent = true; verbose = false;
    }
    else if (current_argument == "--file" || current_argument == "-f") {
      i++; current_argument = argv[i];
      if (current_argument[0] == "-") {
        cout << "Given file starts with a dash (-), making me confusing it with an argument." << endl <<
        "Leaving..." << endl;
      }
      else {
        file_to_swap = current_argument;
      }
    }
    else if (current_argument == "--command" || current_argument == "-c"){
      i++; current_argument = argv[i];
      if (argv[i+1][0] == "-") {
          cout << "I said \"THIS MUST BE THE LAST ARGUMENT\" in the help text." << endl <<
          "Leaving..." << endl;
          exit(1);
      }
      else if (argv[i+1] != NULL) {
        cout << "I just detected that you put the command WITHOUT quotes (\"...\")!" << endl <<
        "This causes trouble because I will hand over \"bash\" only \"" << current_argument << "\" and not the \"" << argv[i+1] << "\" and following parts!" << endl <<
        "Please put this command INSIDE QUOTES! Leaving..." << endl;
        exit(1);
      }
      else {
        command_to_run = argv[i];
      }
    }
    //else ı
  }
}

int main(int argc, char *argv[]){
  launchedas = argv[0];
  if (argv[1] == NULL) {
      cout << "No argument given, leaving..." << endl <<
      "Try seeing help text in \"" << launchedas << " --help\"." << endl;
  }
  argument_check(argc, argv);
}
