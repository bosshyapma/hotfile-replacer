CXX          = g++
CXX_FLAGS    = -Wall -Werror -Wextra
LD           = ld
LN           = ln
RM           = rm
CHMOD        = chmod
GZIP         = gzip
STRIP        = strip
CP           = cp
PROJECT_NAME = hotfile-replacer
WHAT_TO_USE  = $(shell readlink -f ./$(PROJECT_NAME))
IS_INSTL_MAN = false

.PHONY       = target.out clean g++ gcc clang++ clang install clean-compiled clean-check check

g++: target.out
	$(CXX) main.cpp target.out $(CXX_FLAGS) -o g++.a.out # compile
	$(LN) -sfv g++.a.out $(PROJECT_NAME) # recreate link

gcc: g++

clang++: CXX = clang++
clang++: target.out
	$(CXX) main.cpp target.out $(CXX_FLAGS) -o clang++.a.out # compile
	$(LN) -sfv g++.a.out $(PROJECT_NAME) # recreate link

clang: clang++

strip: ; $(STRIP) -s $(WHAT_TO_USE)

target.out:
	$(RM) -rvf target.out target.o
	echo CXX_FLAGS:  $(CXX_FLAGS) > target.o 2>&1
	echo $(CXX) -v:  >> target.o 2>&1
	$(CXX) -v >> target.o 2>&1
	$(LD) -r -b binary -o target.out target.o

manpage: ; $(GZIP) -k -v -c ./$(PROJECT_NAME).1 > ./$(PROJECT_NAME).1.gz

clean: ; $(RM) -rvf $(PROJECT_NAME) *.out *.o *.gz test-folder/ test-folder-old/

clean-compiled: ; $(RM) -rvf $(PROJECT_NAME) *.out *.o *.gz

clean-check: ; $(RM) -rvf test-folder/ test-folder-old/

# check:
#	$(CHMOD) +x ./test-suite.sh
#   WHAT_TO_TEST=$(WHAT_TO_USE) ./test-suite.sh

install: # had a thought on creating an entire bash script just for this section because how make's if-else works
	@echo "--------------------"
ifeq ($(shell PATH="$(PATH):/data/data/com.termux/files/usr/bin" which termux-info), /data/data/com.termux/files/usr/bin/termux-info)
	@echo "Termux installation detected (or \"which termux-info\" lied)"
	@echo "Will install to \"/data/data/com.termux/files/usr/bin/$(PROJECT_NAME)\" no matter what"
	$(CP) -v $(WHAT_TO_USE) /data/data/com.termux/files/usr/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz /data/data/com.termux/files/usr/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
else ifeq ($(shell whoami), root)
	@echo "Root detected, installing to \"/usr/bin/$(PROJECT_NAME)\""
	$(CP) -v $(WHAT_TO_USE) /usr/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz /usr/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
else
	@echo "Non-root detected, installing to \"~/.local/bin/$(PROJECT_NAME)\""
	$(CP) -v $(WHAT_TO_USE) ~/.local/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz ~/.local/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
endif
